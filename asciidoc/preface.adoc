[preface]
== Preface

With the dramatic increases in home computer performance over the past few years it is not surprising that chaos theory and fractal graphics programming have become extremely popular. People can now create, in their own homes, the programming experiments and images which only 10 years ago were leading edge scientific research. The Amiga is an ideal system for exploring this exciting field, as it can perform the necessary calculations very quickly and the output can be displayed using high resolution graphics.

This book details the methods used to produce most important fractals and also provides a wide variety of ideas for further experimentation, using many of the Amiga's most powerful features including colour graphics and even sound. The concepts behind these images are discussed using only the most elementary maths and the minimum of technical jargon. Wider aspects of chaos, such as its history and notable applications in the real world, have also been included.

Fully annotated Amiga BASIC (compatible with Hisoft BASIC) and GFA BASIC example programs are given throughout to illustrate the concepts and to provide a base for experimentation. Such examples can even be appreciated by novice programmers who will find this an enjoyable way to learn more about Amiga programming techniques. Appendices provide useful BASIC routines and give hints for converting the programs to other Amiga languages.

The layout of this book is such that it may be used either as a practical introduction to chaos or as a reference text for the experienced reader, where chapters may be skipped or read out of sequence if necessary.

I would like to thank my parents, Ange and Mike, for their support and assistance during the writing of this book. Also thanks to Gemma for distribution of the support disk, and to Russell for his help with the initial experiments. 

*_Conrad Bessant_*, 1993
