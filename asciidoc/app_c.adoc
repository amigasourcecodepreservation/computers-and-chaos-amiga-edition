﻿== Appendix C

<$cn>Listing Conversion

As mentioned at the beginning of this book, it is now possible to investigate Chaos theory using a range of popular personal computers and computer languages. The Amiga is only one such computer, and Amiga BASIC is only one of the many available languages. This appendix is provided as a guide to converting the Amiga example programs given in this book to other languages, including BBC BASIC, the language used on BBC micro and Archimedes computers.

The conversion notes have been written specifically to include all programming techniques used in this book without digressing into more advanced, unnecessary, material. For each computer language syntax comparison tables are given. These may not on their own allow a program to be converted but they should, in conjunction with the text and example programs, steer you toward a suitable conversion. Because Amiga BASIC specific commands have been avoided wherever possible and because the majority of the programs consist mainly of standard maths and loops, conversion to other BASICs is relatively easy.

<$ha>Acorn BBC and Archimedes Computers

Unlike many other computers, all BBC machines have a very good built-in language, BBC BASIC, which is rich in structure and exploits the computers to the full.

There are two distinct classes of BBC computers, the original eight-bit machines (Model As, Model Bs and Masters) and the newer 32-bit reduced instruction set computer (RISC) machines - the Archimedes. Both computers use syntax compatible versions of BBC BASIC, but the original BBC design is naturally much slower than the new one.

Table C.2 is provided as a rough guide to converting the listings given throughout this book to BBC BASIC. Note that BBC BASIC version five, built into Archimedes machines, provides enhanced programming structures, such as IF...THEN...[ELSE]...ENDIF, and extra commands and functions including mouse pointer position functions. All programs written in early versions of BBC BASIC will, however, work on the Archimedes.

Amiga BASIC				BBC BASIC

REM					REM
*CALL name				PROCname
FOR...NEXT				FOR....NEXT
IF...THEN...[ELSE]...END IF	IF...THEN...[ELSE]
INPUT					INPUT
LINE [(a,b)]-(x,y)		DRAW x,y
MOUSE(0)				Mouse not standard on all BBCs
*PALETTE				VDU 19...
PRINT					PRINT
PSET(x,y)				PLOT 69,x,y
*SCREEN				MODE
*SOUND frequency,duration	SOUND channel,volume,frequency,duration
*SUB name STATIC...END SUB	DEFPROCname...ENDPROC
*WHILE...WEND			REPEAT...UNTIL

*Indicates nearest, not exact, replacement

<$tc>Table C.2: Amiga BASIC conversion table for BBC BASIC.
 
A BBC BASIC version of the Sierpinski triangle program is given in listing C.3 below. This is written so as to be compatible with all versions of BBC BASIC, although it will not run on the BBC model A since it uses screen mode 0.

DIM x%(2), y%(2)

'The following assignments should be changed
'if a screen mode other than 0 is to be used
ScreenWidth% = 640
ScreenHeight% = 256

MODE 0

x%(0) = 0
x%(1) = ScreenWidth% / 2
x%(2) = ScreenWidth%
y%(0) = ScreenHeight%
y%(1) = 0
y%(2) = ScreenHeight%

vertex% = RND(3)-1
px% = x%(vertex)
py% = y%(vertex)

REPEAT
	vertex% = RND(3)-1
	px% = px% + (x%(vertex%) - px%) / 2
	py% = py% + (y%(vertex%) - py%) / 2
	PSET (px%, py%)
UNTIL INKEY$<>""

<$fc>Listing C.3: A BBC BASIC program which produces the Sierpinski triangle

If you are using an Archimedes rather an 8-bit BBC, you will obvious want to maximise your computer's graphics capabilities by running the program in the highest resolution screen mode possible. With a standard monitor this is mode 17, which provides a resolution of 1056x256 pixels. To alter the program to run in mode 17, change the MODE command accordingly and edit the lines which initialise the screen size variables to read as follows:

ScreenWidth% = 1056
ScreenHeight% = 256

<$ha>Using Other Languages on the Amiga

If you are using an Amiga, but Amiga BASIC is not your preferred programming language you will undoubtedly want to know how to convert the example listings in this book to a form suitable for your chosen language.

It would be impossible to discuss all the differences between the popular Amiga languages in this appendix, so instead only the most important differences for fractal work are mentioned. A table to aid BASIC program conversion is shown below.

Amiga BASIC/Hisoft BASIC		GFA BASIC

' or REM				! or REM
*CALL name				GOSUB name
FOR...NEXT				FOR....NEXT
IF...THEN...[ELSE]...END IF	IF...[THEN]...[ELSE]...ENDIF
*IF...THEN...[ELSE]...END IF	SELECT...CASE...DEFAULT...ENDSELECT
INPUT					[FORM] INPUT
LINE [(a,b)]-(x,y)		DRAW [a,b] TO x,y
MOUSE(0)				MOUSEK
*PALETTE				SETCOLOR
PRINT					PRINT
PSET(x,y)				PLOT x,y
*SCREEN				OPENS
*SOUND frequency,duration	SOUND frequency,duration
*SUB name STATIC...END SUB	PROCEDURE name...RETURN	
*WHILE...WEND			REPEAT...UNTIL
WHILE...WEND				WHILE...WEND	
*WINDOW				OPENW

*Indicates nearest, not exact, replacement

<$tc>Table C.3: Syntax comparison of Amiga BASIC and GFA BASIC. All Amiga BASIC programs should work without alteration in Hisoft BASIC.

<$hb>Hisoft BASIC

Hisoft BASIC is intended to be used for the same type of applications as Amiga and GFA BASIC and therefore contains many of the same commands. Hisoft BASIC syntax is refreshingly similar to that used in Amiga BASIC, making program conversion easy, but a host of powerful extra commands are also provided. Hisoft BASIC is well structured, allowing constants and other special variables to be defined, which is very useful in chaos programming. When converting the programs in this book to Hisoft format you should use such special definitions wherever possible to improve speed and clarity.

<$hb>AMOS BASIC and Blitz BASIC

The key point to remember when converting programs to AMOS is that all non-integer variables must be suffixed by a hash character (#). Because AMOS is geared toward game creation, where floating point numbers are rare, it treats all variables as integers unless instructed otherwise in this way. In most other respects AMOS has the same syntax as Amiga BASIC except for a few notable exceptions, the most prominent of which is the use of line numbers and GOSUBs rather than named procedures. The many extra non-standard commands provided by AMOS make some tasks, such as graphics manipulation, much simpler.

This discussion also holds for Blitz BASIC, as it is intended for the same programming tasks as AMOS. Note, however, that Blitz BASIC is compiled and is the fastest BASIC implementation for the Amiga.

<$hb>GFA BASIC

The GFA BASIC interpreter is the ideal language for chaos programming as it is well structured and very fast (faster even than compiled Hisoft BASIC). The fact that a compiler is not necessary to achieve this speed make it very easy to make small changes to programs and see their effect without delay. The easy access to the Amiga's user interface, discussed in chapter five, make GFA BASIC ideal for development of easy to use fractal 'explorers'. If you have a special interest is writing chaos programs in GFA BASIC then I recommend the Atari edition of this book in which all the examples are written in the ST version of GFA BASIC, which is syntax compatible with the Amiga version.

<$hb> C

Although specific applications for C have been cited throughout this book, no example code has been given. Originally I had intended to present all the example programs in both C and BASIC, but it was decided that this would only make the principles of chaos theory harder to grasp. Also, it was assumed that most C programmers would be able to read BASIC and should therefore be able to convert the programs with relative ease. C has the advantage of being generally quicker than BASIC, especially interpreted BASIC, and is also more easily ported between different computers.
